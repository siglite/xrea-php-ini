# .fast-cgi-bin

## php4 (unused)

```
> /usr/local/bin/php4 -v
/usr/local/bin/php4: error while loading shared libraries: libmysqlclient.so.16:
cannot open shared object file: No such file or directory
```

## php52 (only CGI)

```
> /usr/local/bin/php52 -v
PHP 5.2.17 (cgi) (built: Jun 30 2015 18:18:21)
Copyright (c) 1997-2010 The PHP Group
Zend Engine v2.2.0, Copyright (c) 1998-2010 Zend Technologies
```

PHP 5.2 は .user.ini に対応していない。

## php / php5 / php53

```
> /usr/local/bin/php -v
PHP 5.3.29 (cgi-fcgi) (built: Sep 14 2016 12:37:22)
Copyright (c) 1997-2014 The PHP Group
Zend Engine v2.3.0, Copyright (c) 1998-2014 Zend Technologies
> /usr/local/bin/php5 -v
PHP 5.3.29 (cgi-fcgi) (built: Sep 14 2016 12:37:22)
Copyright (c) 1997-2014 The PHP Group
Zend Engine v2.3.0, Copyright (c) 1998-2014 Zend Technologies
> /usr/local/bin/php53 -v
PHP 5.3.29 (cgi-fcgi) (built: Sep 14 2016 12:37:22)
Copyright (c) 1997-2014 The PHP Group
Zend Engine v2.3.0, Copyright (c) 1998-2014 Zend Technologies
```

## php54

## php55

## php56

## php70

## .htaccess

```
AddHandler application/x-httpd-php70cgi .php
AddHandler application/x-httpd-php56cgi .php
AddHandler application/x-httpd-php55cgi .php
AddHandler application/x-httpd-php54cgi .php
AddHandler application/x-httpd-php53cgi .php
AddHandler application/x-httpd-php52cgi .php
```
